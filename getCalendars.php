<?php
	require 'database.php';
	header("Content-Type: application/json");
	session_start();
	$previous_ua = @$_SESSION['useragent'];
	$current_ua = $_SERVER['HTTP_USER_AGENT'];
 
	if(isset($_SESSION['useragent']) && $previous_ua !== $current_ua){
		die("Session hijack detected");
	}else{
		$_SESSION['useragent'] = $current_ua;
	}	
	$calendars = array();
	if(isset($_SESSION['username'])){
		$user_id = $_SESSION['user_id'];
		$stmt = $mysqli->prepare('select cal_name from calendar where user_id = ?');
		if(!$stmt){
			echo json_encode(array(
				"success" => false,
				"message" => $mysqli->error
				));
			exit;		
		}
		$stmt->bind_param('i', $user_id);
		$stmt->execute();
		$stmt->bind_result($cal_name);
		while($stmt->fetch()){
			$calendars[] = $cal_name;
		};
		$stmt->close();
		echo json_encode($calendars);
		exit;
	}
	
?>