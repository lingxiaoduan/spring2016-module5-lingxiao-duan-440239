
<?php
/*
 * addEvent.php - this file adds events to the database
 * new calendars are added at the same time as an event if the user fills in the form field
 * new calendars are made concurrent with adding a new event because we believe a new calendar will
 * only be made when a user has a new event they do not yet have a category for.
 * this allows the user to create a new event and put it in the new calendar in one easy step.
 */ 
	require 'database.php';
	header("Content-Type: application/json");
	session_start();
	$previous_ua = @$_SESSION['useragent'];
	$current_ua = $_SERVER['HTTP_USER_AGENT'];
 
	if(isset($_SESSION['useragent']) && $previous_ua !== $current_ua){
		die("Session hijack detected");
	}else{
		$_SESSION['useragent'] = $current_ua;
	}
	/* check if a user is logged in *REQUIRED* to be able to add events */
	if(isset($_SESSION['user_id'])){
		$user_id = $_SESSION['user_id'];
		if(isset($_POST['new_cal'])){
			$new_cal = $_POST['new_cal'];
			if($new_cal == ''){
				echo json_encode(array(
					"success" => false,
					"message" => "please name your new calendar"
				));
				exit;
			}
			else{
				/* check if a calendar by the requested name exists already -- repeated names are not allowed */
				$stmt = $mysqli->prepare('select count(*) from calendar where cal_name = ? and user_id = ?');
				if(!$stmt){
					echo json_encode(array(
						"success" => false,
						"message" => "321an error occured"
					));
					exit;
				}
				$stmt->bind_param('si', $new_cal, $user_id);
				$stmt->execute();
				$stmt->bind_result($count);
				$stmt->fetch();
				$stmt->close();
				if($count > 0){
					echo json_encode(array(
						"success" => false,
						"message" => "that calendar already exists"
					));
					exit;
				}
				/* add the new calendar for the user */
				$stmt = $mysqli->prepare('insert into calendar (user_id, cal_name) values (?, ?)');
				if(!$stmt){
					echo json_encode(array(
						"success" => false,
						"message" => "222an error occured"
					));
					exit;
				}
				$stmt->bind_param('is', $user_id, $new_cal);
				$stmt->execute();
				$stmt->close();
			}
		}
		
		/* make sure all the required form fields are set */
		if(isset($_POST['event_name']) && isset($_POST['date_year']) && isset($_POST['date_month']) && isset($_POST['date_day']) && isset($_POST['event_time'])){
			/* '' is the default name for calendar (in the options it is equivalent to '--') */
			$cal = '';
			
			/* a new calendar request was made. submit the new event tied to the new calendar */
			if(isset($_POST['new_cal'])){
				$cal = $_POST['new_cal'];
			}
			if(isset($_POST['cal_name'])){
				if($_POST['cal_name'] == 'default'){
					$cal = '';
				}
				else{
					$cal = $_POST['cal_name'];
				}
			}
			/* if call is not an empty string, there was a requested calendar that the event should be tied to */
			/* put this event in the secified calendar by getting the calendar's id */
			if($cal != ''){
				$stmt = $mysqli->prepare('select id from calendar where user_id = ? and cal_name = ?');
				if(!$stmt){
					echo json_encode(array(
						"success" => false,
						"message" => "123an error occured"
					));
					exit;
				}
				$stmt->bind_param('is', $user_id, $cal);
				$stmt->execute();
				$stmt->bind_result($cal_id);
				$stmt->fetch();
				$stmt->close();
			}
			/* default calendar has no calendar id so we insert the event with a null cal_id */
			else{
				$cal_id = null;
			}
			$event_name = $_POST['event_name'];
			$event_catgory = $_POST['cat'];
			$event_year = $_POST['date_year'];
			$event_month = $_POST['date_month'];
			$event_day = $_POST['date_day'];
			$event_time = $_POST['event_time'];
			$event_desc = $_POST['event_desc'];
			$event_date_str = "";
			
			/* check the format of the submitted year (must be 4 digits) */
			if(strlen($event_year) == 4){
				$event_date_str = $event_date_str.$event_year;
			}
			else{
				echo json_encode(array(
					"success" => false,
					"message" => "check date format"
				));
				exit;
			}
			/* check if the format of the month is correct and is a valid month number */
			if(strlen($event_month) == 2 && (int)$event_month > 0 && (int)$event_month < 13){
				$event_date_str = $event_date_str.$event_month;
			}
			else{
				echo json_encode(array(
					"success" => false,
					"message" => "check date format"
				));
				exit;
			}
			/* check the day format is correct */
			if(strlen($event_day) == 2){
				/* make sure the number is a valid value for the given month */
				/* check the months with 31 days */
				if((int)$event_month == 1 || (int)$event_month == 3 || (int)$event_month == 5 || (int)$event_month == 7 || (int)$event_month == 8 || (int)$event_month == 10 || (int)$event_month == 12){
					if((int)$event_day < 32 && (int)$event_day > 0){
						$event_date_str = $event_date_str.$event_day;
					}
				}
				else{
					/* check february for leap year */
					if((int)$event_month == 2){
						/* if it's a leap year, february has 29 days */
						if((int)$event_year % 4 == 0){
							if((int)$event_day < 30 && (int)$event_day > 0){
								$event_date_str = $event_date_str.$event_day;
							}
						}
						else{
							/* if it's not a leap year, february only has 28 days */
							if((int)$event_Day < 29 && (int)$event_day > 0){
								$event_date_str = $event_date_str.$event_day;
							}
						}
					}
					/* if it's not february then the remaining months only have 30 days */
					if((int)$event_day < 31 && (int)$event_day > 0){
						$event_date_str = $event_date_str.$event_day;
					}
					else{
						echo json_encode(array(
							"success" => false,
							"message" => "check date"
						));
						exit;
					}
				}
			}
			else{
				echo json_encode(array(
					"success" => false,
					"message" => "check date"
				));
				exit;
			}
		}
		else{
			echo json_encode(array(
				"success" => false,
				"message" => "check form fields"
			));
			exit;
		}
		/* create the date id for the event by casting the string to an int */
		/* insert the event into the table */
		$event_date = (int)$event_date_str;
		$stmt = $mysqli->prepare("insert into events (user_id, event_name, event_date,event_cat, event_desc, event_time, cal_id) values (?, ?, ?,?, ?, ?, ?)");
		if(!$stmt){
			echo json_encode(array(
		        "success" => false,
		        "message" => "an error occured, please try again"
		    ));
			exit;
		}
		$stmt->bind_param('isisssi', $user_id, $event_name, $event_date, $event_catagory, $event_desc, $event_time, $cal_id);
		$stmt->execute();
		$stmt->close();
		echo json_encode(array(
			"success" => true,
			"message" => "add event successful",
			"event_name" => $event_name,
			"event_date" => $event_date,
			"event_time" => $event_time,
			"event_desc" => $event_desc
		));
		exit;
	}
	else{
		echo json_encode(array(
			"success" => false,
			"message" => "please log in to add event"
		));
		exit;
	}
?>