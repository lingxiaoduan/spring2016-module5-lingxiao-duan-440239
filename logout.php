<?php
#require 'database.php';
header("Content-Type: application/json");
session_start();
$previous_ua = @$_SESSION['useragent'];
$current_ua = $_SERVER['HTTP_USER_AGENT'];
 
if(isset($_SESSION['useragent']) && $previous_ua !== $current_ua){
	die("Session hijack detected");
}else{
	$_SESSION['useragent'] = $current_ua;
}
if(isset($_SESSION['username'])){
	session_destroy();
	echo json_encode(array(
		"success" => true,
		"message" => "You have successfully logged out."
	));
	exit;
}
else{
	echo json_encode(array(
		"success" => false,
		"message" => "an error occured, please try again."
	));
}
?>